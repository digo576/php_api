<?php
/**
 * Developer: Rodrigo Marques
 * Date: 02/02/19
 *
 * Refatore o código abaixo, fazendo as alterações que julgar necessário.
 *
 */

/**
 * Código antigo
 */
class MyUserClass
{
    public function getUserList()
    {
        $dbconn = new DatabaseConnection('localhost', 'user', 'password');
        $results = $dbconn->query('select name from user');

        sort($results);

        return $results;
    }
}

/**
 * Código refatorado.
 *
 * Por medidas de segurança, vamos supor que as variáveis "nome do banco", "user" e "password" foram passadas para
 * variáveis de ambiente. Assim, ocultamos essa informação do código, diminuindo as vulnerabilidades.
 *
 */
class MinhaClasseUsuario
{
    public function getListaUsuarios()
    {
        try {
            $dbConnection = new DatabaseConnection($_SERVER["DB_NAME"], $_SERVER["DB_USER"], $_SERVER["DB_PASSWORD"]);

            $sql = 'SELECT name FROM user ORDER BY name';
            return $dbConnection->query($sql);
        } catch (PDOException $exception) {
            echo "Falha ao conectar ao banco de dados!";
            return null;
        }
    }
}

