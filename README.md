# Supero Challange

Este repositório é para um desafio realizado pela Supero.

# Instalações

> Obs: Essas instruções foram testas em um ambiente Linux Ubuntu 18.04 LTS

Para rodar arquivos PHP por linha de comando, será necessário a dependência `php7.2-cli`. Para isto, rode o seguinte comando:

```SHELL
$ sudo apt install php7.2-cli
```

Para rodar o projeto da API, será necessário as algumas dependências. Para instala-las, rode o seguinte comando:

```SHELL
$ sudo add-apt-repository -y ppa:ondrej/php
$ sudo apt-get update
$ sudo apt-get install -y php7.1 php7.1-fpm libapache2-mod-php7.0 php7.1-cli php7.1-curl php7.1-mysql php7.1-sqlite3 php7.1-gd php7.1-xml php7.1-mcrypt php7.1-mbstring php7.1-iconv php7.1-zip apache2
```

*Observação: Esse projeto utiliza a versão PHP 7.1. Sendo assim, altere sua versão para 7.1 com o seguinte comando:*

```SHELL
$ sudo update-alternatives --config php
```

Após isto, é necessário alterar algumas configurações do Apache com o seguinte comando:
```SHELL
$ sudo a2enmod rewrite
$ sudo systemctl restart apache2
```

Para o banco de dados, é necessário instalar o MySQL. Para isto, execute o seguinte comando:

```SHELL
$ sudo apt install mysql-server
```

Instalar o composer e habilitar o comando `laravel`:
```SHELL
$ curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer
$ sudo chown -R $USER ~/.composer/
$ composer global require "laravel/installer"
$ echo 'export PATH="$HOME/.composer/vendor/bin:$PATH"' >> ~/.bashrc
$ source ~/.bashrc
```

## Primeiro desafio

> Escreva um programa que imprima números de 1 a 100. Mas, para múltiplos de 3 imprima “Fizz” em vez do número e para múltiplos de 5 imprima “Buzz”. Para números múltiplos de ambos (3 e 5), imprima “FizzBuzz”.

Para rodar a resposta deste desafio, basta rodar o seguinte comando na raiz do projeto:

```SHELL
$ php fizzBuzz.php
```

## Segundo desafio

>  Refatore o código abaixo, fazendo as alterações que julgar necessário.

```php
if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
    header("Location: http://www.google.com");
    exit();
} elseif (isset($_COOKIE['Loggedin']) && $_COOKIE['Loggedin'] == true) {
    header("Location: http://www.google.com");
    exit();
}
```

A resposta para este desafio se encontra no arquivo `segundoDesafio.php`.


## Terceiro desafio

>  Refatore o código abaixo, fazendo as alterações que julgar necessário.

```php
class MyUserClass
{
    public function getUserList()
    {
        $dbconn = new DatabaseConnection('localhost', 'user', 'password');
        $results = $dbconn->query('select name from user');

        sort($results);

        return $results;
    }
}
```

A resposta para este desafio se encontra no arquivo `terceiroDesafio.php`.

## Quarto desafio


### Descrição
> Desenvolva uma API Rest para um sistema gerenciador de tarefas (inclusão/alteração/exclusão). 
> As tarefas consistem em `título` e `descrição`, ordenadas por `prioridade`.
> Desenvolver utilizando:
> - Linguagem PHP (ou framework PHP);
> - Banco de dados MySQL;

> Diferenciais:
> - Criação de interface para visualização da lista de tarefas;
> - Interface com drag and drop;
> - Interface responsiva (desktop e mobile);

### Configurando o projeto

Para rodar o projeto é necessário ter o framework Laravel instalado. Para isto, basta seguir os passos de instalação citados inicialmente.

Crie um banco de dados para aplicação através do MySQL commandline ou da forma que você achar melhor.

Para criar um banco de dados por linha de comando, execute os seguintes comandos:

```SHELL
$ sudo mysql -u root
$ mysql > create database supero;
```

Desde a versão 5.6 do PHP não é possível acessar o banco como root. Sendo assim, é necessário criar um novo usuário com privilégios.
Para isto, rode os seguintes comandos:
```SHELL
$ sudo mysql -u root
$ mysql > CREATE USER 'novousuario'@'localhost' IDENTIFIED BY 'password';
$ mysql > GRANT ALL PRIVILEGES ON * . * TO 'novousuario'@'localhost';
$ mysql > FLUSH PRIVILEGES;;
```

Após isto, coloque as informações de banco e usuário no arquivo `.env`. No meu caso, ficou assim:

```TEXT
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=supero
DB_USERNAME=rodrigo
DB_PASSWORD=password
```

Para criar os models, utilize o seguinte comando:

```SHELL
$ php artisan migrate
```

Para realizar as requisições à aplicação, sugiro a aplicação [Postman](https://www.getpostman.com/).

### Rodando o projeto

Após instala-lo, utilize o seguinte comando para rodar o projeto na raiz do projeto `task-manager-api`:

```SHELL
$ php artisan serve
```

### Listando todas as Tarefas

Para listar todas as `Tarefas`, utilize a seguinte requisição:

```TEXT
GET /api/tarefas HTTP/1.1
Host: 127.0.0.1:8000
Accept: application/json
cache-control: no-cache
```

### Criando uma Tarefa

Para criar uma `Tarefa`, basta utilizar a seguinte requisição:

```TEXT
POST /api/tarefas HTTP/1.1
Host: 127.0.0.1:8000
Accept: application/json
cache-control: no-cache
Content-Type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW

Content-Disposition: form-data; name="titulo"

Minha nova tarefa

Content-Disposition: form-data; name="descricao"

Uma descrição para a minha tarefa

Content-Disposition: form-data; name="prioridade"

10
------WebKitFormBoundary7MA4YWxkTrZu0gW--
```

### Exibindo uma Tarefa

Para exibir uma `Tarefa`, basta você saber o `id` dela e passar como argumento, por exemplo:

```TEXT
GET /api/tarefas/{id}
```

No Postman, utilize a requisição:

```TEXT
GET /api/tarefas/2 HTTP/1.1
Host: 127.0.0.1:8000
Accept: application/json
cache-control: no-cache
```

### Atualizando uma tarefa

Para atualizar uma `Tarefa`, basta você saber o `id` dela e passar como argumento, por exemplo:

```TEXT
PATCH /api/tarefas/{id}
``` 

No Postman, utilize a requisição:

```TEXT
PATCH /api/tarefas/1 HTTP/1.1
Host: 127.0.0.1:8000
Accept: application/json
cache-control: no-cache
Postman-Token: e377f2dc-3f5b-4301-8d5d-b889fc84ede7
Content-Type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW

Content-Disposition: form-data; name="titulo"

Tarefa atualizada

Content-Disposition: form-data; name="descricao"

Esta tarefa foi atualizada

Content-Disposition: form-data; name="prioridade"

10
------WebKitFormBoundary7MA4YWxkTrZu0gW--
```

### Remover uma tarefa

Para remover uma `Tarefa`, basta você saber o `id` dela e passar como argumento, por exemplo:

```TEXT
DELETE /api/tarefas/{id}
```

No Postman, utilize a requisição:

```TEXT
DELETE /api/tarefas/2 HTTP/1.1
Host: 127.0.0.1:8000
Accept: application/json
cache-control: no-cache
Postman-Token: 0b19e86f-9711-4ee8-89fb-f004e1c74396
```