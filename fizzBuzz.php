<?php
/**
 * Developer: Rodrigo Marques
 * Date: 02/02/19
 *
 * Escreva um programa que imprima números de 1 a 100. Mas, para múltiplos de 3 imprima “Fizz” em vez do número e
 * para múltiplos de 5 imprima “Buzz”. Para números múltiplos de ambos (3 e 5), imprima “FizzBuzz”.
 *
 * OBS: Para ser múltiplo de 3 e 5, então deverá ser múltiplo de 15.
 *
 */

/**
 * Função que imprime "Fizz" quando o número passado como argumento for mútiplo de 3.
 *
 * @param $numero
 * @return string
 */
function retornaFizzSeMultiploDeTres($numero)
{
    $modulo = $numero % 3;
    return $modulo === 0 ? 'Fizz' : '';
}

/**
 *
 * Função que imprime "Buzz" quando o número passado como argumento for mútiplo de 5.
 *
 * @param $numero
 * @return string
 */
function retornaBuzzSeMultiploDeCinco($numero)
{
    $modulo = $numero % 5;
    return $modulo === 0 ? 'Buzz' : '';
}

/**
 *
 * Função que imprime "Fizz" quando o número passado como argumento for mútiplo de 15.
 *
 * @param $numero
 * @return string
 */
function retornaBuzzSeMultiploDeQuinze($numero)
{
    $modulo = $numero % 15;
    return $modulo === 0 ? 'FizzBuzz' : '';
}

for ($i = 1; $i <= 100; $i++) {
    echo "Número {$i}: ";

    $multiploQuinze = retornaBuzzSeMultiploDeQuinze($i);
    if ($multiploQuinze) {
        echo "{$multiploQuinze} \n";
        continue;
    }

    $multiploCinco = retornaBuzzSeMultiploDeCinco($i);
    if ($multiploCinco) {
        echo "{$multiploCinco} \n";
        continue;
    }

    $multiploTres = retornaFizzSeMultiploDeTres($i);
    if ($multiploTres) {
        echo "{$multiploTres} \n";
        continue;
    }

    echo "\n";

}