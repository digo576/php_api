<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Tarefa;
use App\Http\Resources\TarefaResource;

class TarefaController extends Controller
{
    /**
     * Exibe lista de tarefas por prioridade.
     */
    public function index()
    {
        return TarefaResource::collection(Tarefa::all());
    }

    /**
     * Cria uma nova Tarefa.
     */
    public function store(Request $request)
    {
        $tarefa = Tarefa::create([
            'titulo' => $request->titulo,
            'descricao' => $request->descricao,
            'prioridade' => $request->prioridade,
        ]);

        return new TarefaResource($tarefa);
    }

    /**
     * Exibe uma dada Tarefa.
     */
    public function show(Tarefa $tarefa)
    {
        return new TarefaResource($tarefa);
    }

    /**
     * Atualiza uma Tarefa.
     */
    public function update(Request $request, Tarefa $tarefa)
    {
        $tarefa->update($request->only(['titulo', 'descricao', 'prioridade']));

        return new TarefaResource($tarefa);
    }

    /**
     * Apaga uma Tarefa.
     */
    public function destroy(Tarefa $tarefa)
    {
        $tarefa->delete();
        return response()->json(null, 204);
    }
}
