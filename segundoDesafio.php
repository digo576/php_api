<?php
/**
 * Developer: Rodrigo Marques
 * Date: 02/02/19
 *
 * Refatore o código abaixo, fazendo as alterações que julgar necessário.
 *
 */

function codigoAntigo()
{
    if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
        header("Location: http://www.google.com");
        exit();
    } elseif (isset($_COOKIE['Loggedin']) && $_COOKIE['Loggedin'] == true) {
        header("Location: http://www.google.com");
        exit();
    }
}

/**
 * Refatoração do código segundo desafio.
 *
 * Verifica se o usuário está logado. A verificação é feita através da sessão ou cookies.
 *
 */
function codigoRefatorado()
{
    $usuarioEstaLogado = (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) || (isset($_COOKIE['Loggedin']) && $_COOKIE['Loggedin'] == true);

    if ($usuarioEstaLogado) {
        header("Location: http://www.google.com");
        exit();
    }
}